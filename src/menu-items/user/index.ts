import aboutPage from './aboutPage';
import homePage from './homePage';
import blogPage from './blogPage';

export interface groupMenu {
    id: string;
    title: string;
    type: string;
    children?: itemMenu[];
}

export interface itemMenu {
    id: string;
    title: string;
    type: string;
    url: string;
    breadcrumbs: boolean;
    target?: boolean;
}

// ==============================|| MENU ITEMS ||============================== //

const userMenuItems = {
    items: [homePage, blogPage, aboutPage]
};

export default userMenuItems;