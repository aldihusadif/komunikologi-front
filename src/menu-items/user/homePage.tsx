// ==============================|| HOME PAGE MENU ITEMS ||============================== //

const homePage = {
  id: "home-page",
  title: "Home Page",
  type: "group",
  children: [
    {
      id: "home",
      title: "Home",
      type: "item",
      url: "/home",
      breadcrumbs: false,
    },
  ],
};

export default homePage;
