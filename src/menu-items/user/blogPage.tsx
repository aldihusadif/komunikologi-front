// ==============================|| ABOUT US PAGE MENU ITEMS ||============================== //

const blogPage = {
  id: "blog-page",
  title: "Blog Page",
  type: "group",
  children: [
    {
      id: "blog",
      title: "Blog",
      type: "item",
      url: "/blog",
      breadcrumbs: false,
    },
  ],
};

export default blogPage;
