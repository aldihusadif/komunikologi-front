// ==============================|| ABOUT US PAGE MENU ITEMS ||============================== //

const aboutPage = {
  id: "about-page",
  title: "About Page",
  type: "group",
  children: [
    {
      id: "about-us",
      title: "About Us",
      type: "item",
      url: "/about-us",
      breadcrumbs: false,
    },
  ],
};

export default aboutPage;
