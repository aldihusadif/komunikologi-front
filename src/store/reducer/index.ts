import { combineReducers } from "redux";
import customizationReducer, { ICustomizationReducer } from "./customizationReducer";

export interface IReducer {
    customization: ICustomizationReducer
}

const reducer = combineReducers({
    customization: customizationReducer
})

export default reducer