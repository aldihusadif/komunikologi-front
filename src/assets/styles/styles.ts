import { CSSObject } from "@mui/styled-engine-sc";

export const bgSecondary = {
    backgroundColor: '#000428 !important'
} as CSSObject

export const bgAccent1 = {
    backgroundColor: '#ECECEC'
} as CSSObject

export const colorSecondary = {
    color: '#000428'
} as CSSObject

export const colorAccent1 = {
    color: '#928DAB'
} as CSSObject