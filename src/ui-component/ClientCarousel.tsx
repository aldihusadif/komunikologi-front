import { Box, Paper } from "@mui/material";
import Carousel from "react-material-ui-carousel";

interface IClientCarousel {
  imgCarousel: itemCarousel[];
}

interface itemCarousel {
  img1: string;
  img2: string;
  img3: string;
  img4: string;
}

export default function ClientCarousel({ imgCarousel }: IClientCarousel) {
  const ItemCarousel = ({ img1, img2, img3, img4 }: itemCarousel) => {
    return (
      <Paper
        className="grid grid-cols-1 sm:grid-cols-1 lg:grid-cols-4 gap-8 items-center"
        elevation={0}
      >
        <Box className="flex justify-center">
          <img src={img1} />
        </Box>
        <Box className="flex justify-center">
          <img src={img2} />
        </Box>
        <Box className="flex justify-center">
          <img src={img3} />
        </Box>
        <Box className="flex justify-center">
          <img src={img4} />
        </Box>
      </Paper>
    );
  };

  return (
    <Carousel fullHeightHover={false} autoPlay={false} className="py-9">
      {imgCarousel.map((item, idx) => (
        <ItemCarousel
          key={idx}
          img1={item.img1}
          img2={item.img2}
          img3={item.img3}
          img4={item.img4}
        />
      ))}
    </Carousel>
  );
}
