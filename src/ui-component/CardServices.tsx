import { Box, Divider, Typography } from "@mui/material";
import { colorSecondary } from "../assets/styles/styles";

interface ICardServices {
  lead: string;
  icon: string;
}

export default function CardServices({ icon, lead }: ICardServices) {
  return (
    <Box
      className=" rounded-2xl border-2 p-5"
      sx={{ backgroundColor: "inherit", borderColor: "#000428" }}
    >
      <img src={icon} alt="communications" />
      <Typography className="mt-4 font-bold" sx={colorSecondary}>
        {lead}
      </Typography>
      <Divider className="py-2" />
    </Box>
  );
}
