import { Instagram, LinkedIn, WhatsApp } from "@mui/icons-material";
import { Box, IconButton, Typography } from "@mui/material";
import { bgSecondary } from "src/assets/styles/styles";
import { colorSecondary } from "../assets/styles/styles";

interface ICardProfile {
  name: string;
  title: string;
  urlImage: string;
}

export default function CardProfile({ name, title, urlImage }: ICardProfile) {
  return (
    <Box sx={{ borderRadius: 10 }} className="p-8 shadow-lg">
      <Box>
        <img src={urlImage} alt="" style={{ backgroundImage: "none" }} />
      </Box>
      <Typography
        className="leading-normal font-bold text-base"
        sx={colorSecondary}
      >
        {name}
      </Typography>
      <Typography
        className="leading-normal font-light text-sm"
        sx={colorSecondary}
      >
        {title}
      </Typography>
      <Box className="grid grid-cols-3 gap-4 justify-items-center mt-8">
        <Box className="flex items-center">
          <IconButton sx={bgSecondary} className="text-white">
            <LinkedIn />
          </IconButton>
        </Box>
        <Box className="flex items-center">
          <IconButton sx={bgSecondary} className="text-white">
            <Instagram />
          </IconButton>
        </Box>
        <Box className="flex items-center">
          <IconButton sx={bgSecondary} className="text-white">
            <WhatsApp />
          </IconButton>
        </Box>
      </Box>
    </Box>
  );
}
