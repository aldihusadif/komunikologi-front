import { Box, Typography } from "@mui/material";
import { colorSecondary } from "src/assets/styles/styles";
import { colorAccent1 } from "../assets/styles/styles";

interface ICardBlogPrimary {
  urlImage: string;
  titleBlog: string;
  authorBlog: string;
}

export default function CardBlogSecondary({
  urlImage,
  titleBlog,
  authorBlog,
}: ICardBlogPrimary) {
  return (
    <Box className="p-4 shadow-sm rounded-2xl lg:shadow-md">
      <Box className="flex items-center lg:items-start">
        <img
          src={urlImage}
          alt={titleBlog}
          className="w-36 h-auto rounded-2xl mr-3 lg:w-60 lg:mr-4"
        />
        <Box>
          <Typography
            className="mb-4 font-extrabold text-left text-sm lg:text-lg"
            sx={colorSecondary}
          >
            {titleBlog}
          </Typography>
          <Typography className="text-xs md:text-base" sx={colorAccent1}>
            {authorBlog}
          </Typography>
        </Box>
      </Box>
    </Box>
  );
}
