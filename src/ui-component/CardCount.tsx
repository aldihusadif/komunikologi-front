import { Box, Divider, Typography } from "@mui/material";
import { colorSecondary } from "../assets/styles/styles";

interface ICardCount {
  lead: string;
  icon: string;
  count: string;
}

export default function CardCount({ icon, lead, count }: ICardCount) {
  return (
    <Box
      className="rounded-2xl border-2 p-5 flex items-center"
      sx={{
        flexDirection: "column",
        background: "linear-gradient(145deg, #000428 42.71%, #004E92 100%)",
        borderColor: "#000428",
      }}
    >
      <img src={icon} alt="communications" className="w-24" />
      <Typography className="text-5xl font-extrabold text-white mt-4">
        {count} <span className="text-3xl">+</span>
      </Typography>
      <Typography
        className="mt-4 font-bold text-white leading-normal text-xl"
        sx={colorSecondary}
      >
        {lead}
      </Typography>
      <Divider className="py-2" />
    </Box>
  );
}
