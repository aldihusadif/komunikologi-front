import { Avatar, Box, Typography } from "@mui/material";
import { colorSecondary } from "src/assets/styles/styles";
import { colorAccent1 } from "../assets/styles/styles";

interface ICardBlogPrimary {
  urlImage: string;
  titleBlog: string;
  leadBlog: string;
  authorBlog: string;
  ocupationAuthor: string;
}

export default function CardBlogPrimary({
  urlImage,
  titleBlog,
  leadBlog,
  authorBlog,
  ocupationAuthor,
}: ICardBlogPrimary) {
  return (
    <Box className="p-4 shadow-sm rounded-2xl lg:shadow-md">
      <Box className="mb-4">
        <img src={urlImage} alt="" className="rounded-2xl" />
      </Box>
      <Typography
        className="mb-4 font-extrabold text-left text-base lg:text-lg"
        sx={colorSecondary}
      >
        {titleBlog}
      </Typography>
      <Typography
        className="mb-4 text-justify text-sm font-light lg:text-base"
        sx={{ ...colorAccent1, whiteSpace: "normal" }}
      >
        {leadBlog}
      </Typography>
      <Box display={"flex"} alignItems={"center"}>
        <Avatar />
        <Box className="ml-4">
          <Typography
            className="text-sm md:text-base lg-text-lg"
            sx={colorSecondary}
          >
            {authorBlog}
          </Typography>
          <Typography
            className="text-xs md:text-sm lg:text-base"
            sx={colorAccent1}
          >
            {ocupationAuthor}
          </Typography>
        </Box>
      </Box>
    </Box>
  );
}
