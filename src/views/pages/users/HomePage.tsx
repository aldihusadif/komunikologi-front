import { Box, Button, Typography } from "@mui/material";
import imgHeroes from "src/assets/images/heroes-bg.svg";
import {
  bgAccent1,
  colorAccent1,
  colorSecondary,
} from "../../../assets/styles/styles";
import aboutImg1 from "src/assets/images/about-img1.svg";
import aboutImg2 from "src/assets/images/about-img2.svg";
import aboutImg3 from "src/assets/images/about-img3.svg";
import { ArrowForward } from "@mui/icons-material";
import CardServices from "src/ui-component/CardServices";
import services1 from "src/assets/icons/opportunity.svg";
import services2 from "src/assets/icons/analysis.svg";
import services3 from "src/assets/icons/conference.svg";
import services4 from "src/assets/icons/bullhorn.svg";
import CardCount from "src/ui-component/CardCount";
import count1 from "src/assets/icons/countImg1.svg";
import count2 from "src/assets/icons/countImg2.svg";
import count3 from "src/assets/icons/countImg3.svg";
import ClientCarousel from "src/ui-component/ClientCarousel";
import clientImg1 from "src/assets/images/clients/clientImg1.svg";
import clientImg2 from "src/assets/images/clients/clientImg2.svg";
import clientImg3 from "src/assets/images/clients/clientImg3.svg";
import clientImg4 from "src/assets/images/clients/clientImg4.svg";
import clientImg5 from "src/assets/images/clients/clientImg5.svg";
import clientImg6 from "src/assets/images/clients/clientImg6.svg";
import clientImg7 from "src/assets/images/clients/clientImg7.svg";
import clientImg8 from "src/assets/images/clients/clientImg8.svg";
import clientImg9 from "src/assets/images/clients/clientImg9.svg";
import clientImg10 from "src/assets/images/clients/clientImg10.svg";
import clientImg11 from "src/assets/images/clients/clientImg11.svg";
import clientImg12 from "src/assets/images/clients/clientImg12.svg";
import CardProfile from "src/ui-component/CardProfile";
import teamImg1 from "src/assets/images/anriko-caesar-yerico.svg";
import teamImg2 from "src/assets/images/dini-ang-dharmawan.svg";
import teamImg3 from "src/assets/images/adinda-joko-sanjoto.svg";
import teamImg4 from "src/assets/images/irvan-rahmat.svg";

export default function HomePage() {
  const img = [
    {
      img1: clientImg1,
      img2: clientImg2,
      img3: clientImg3,
      img4: clientImg4,
    },
    {
      img1: clientImg5,
      img2: clientImg6,
      img3: clientImg7,
      img4: clientImg8,
    },
    {
      img1: clientImg9,
      img2: clientImg10,
      img3: clientImg11,
      img4: clientImg12,
    },
  ];

  return (
    <Box>
      {/* heroes section */}
      <Box className="grid grid-cols-1 gap-4 items-center px-8 my-10 md:px-20 md:grid-cols-2">
        <Box className="order-last md:order-first mt-4 md:mt-0">
          <Typography
            className="text-2xl font-bold md:text-5xl"
            sx={colorSecondary}
          >
            We Help Great <br /> Companies Grow Up
          </Typography>
          <Typography className="text-sm mt-4 md:text-base" sx={colorAccent1}>
            Our services can encompass any combination of the following
          </Typography>
          <Button
            className="mt-5 text-white capitalize flex-shrink-0 rounded-2xl py-2 px-4 w-auto md:mt-9 md:px-10 md:py-3"
            sx={{
              backgroundImage: `linear-gradient(to left, #000428, #004e92) !important`,
              lineHeight: "normal",
              fontStyle: "normal",
              fontWeight: 700,
            }}
          >
            Schedule a consultant
          </Button>
        </Box>
        <Box className="flex justify-end -mt-10 md:mt-0">
          <img src={imgHeroes} alt="komunikologi-indonesia" />
        </Box>
      </Box>

      {/* about section */}
      <Box className="px-8 my-10 sm:px-20">
        <Box className="mb-5 sm:mb-10">
          <Typography className="text-xl font-bold lg:text-3xl">
            About Us
          </Typography>
          <Typography
            className="text-base font-medium lg:text-xl"
            sx={colorAccent1}
          >
            We are more than communication agency
          </Typography>
        </Box>
        <Box className="grid grid-cols-1 gap-10 items-center md:grid-cols-2">
          <Box>
            <img
              src={aboutImg2}
              alt="komunikologi-indonesia"
              className="w-full"
            />
            <Typography
              className="text-xl font-bold leading-normal mt-5 sm:mt-10 sm:text-4xl"
              sx={colorSecondary}
            >
              Who are we?
            </Typography>
            <Typography
              className="text-sm text-justify leading-normal font-normal mt-2 sm:mt-5 sm:text-base"
              sx={colorAccent1}
            >
              Kogi is an integrated consulting and communication strategy
              services company. Communication is one of the most important parts
              in organizational and individual life. Kogi is present as a
              professional and reliable partner.
            </Typography>
            <Button
              className="mt-4 w-40 rounded-2xl flex-shrink-0 capitalize sm:mt-8 sm:w-44 sm:h-10"
              sx={{
                border: "1px solid #000428",
                color: colorSecondary,
                ":hover": {
                  border: "1px solid #000428",
                },
              }}
              variant="outlined"
              endIcon={<ArrowForward />}
            >
              Read more
            </Button>
          </Box>
          <Box>
            <img
              src={aboutImg1}
              alt="komunikologi-indonesia"
              className="w-full"
            />
            <img
              src={aboutImg3}
              alt="komunikologi-indonesia"
              className="w-full mt-4"
            />
            <Typography
              className="text-xl font-bold leading-normal mt-5 sm:mt-10 sm:text-4xl"
              sx={colorSecondary}
            >
              What we do?
            </Typography>
            <Typography
              className="text-sm text-justify leading-normal font-normal mt-2 sm:mt-5 sm:text-base"
              sx={colorAccent1}
            >
              To achieve ideal user experience for your audience. We recommend
              conducting user research, experience and testing activities to
              gather insights and identify opportunities.
            </Typography>
            <Button
              className="mt-4 w-40 rounded-2xl flex-shrink-0 capitalize text-white sm:w-44 sm:h-10 sm:mt-8"
              sx={{
                backgroundImage: `linear-gradient(to left, #000428, #004e92) !important`,
              }}
              endIcon={<ArrowForward />}
            >
              Read more
            </Button>
          </Box>
        </Box>
      </Box>

      {/* Our Services */}
      <Box sx={{ backgroundColor: bgAccent1 }} className="px-8 py-10 sm:px-20">
        <Box>
          <Typography className="text-xl lg:text-3xl font-bold">
            Our Services
          </Typography>
          <Typography
            className="text-base lg:text-xl font-medium"
            sx={colorAccent1}
          >
            We are master of communications
          </Typography>
        </Box>
        <Box className="grid grid-cols-1 gap-4 mt-4 lg:grid-cols-4 lg:mt-10">
          <CardServices
            icon={services1}
            lead={`Communications chance for management`}
          />
          <CardServices icon={services2} lead={`Training Development`} />
          <CardServices icon={services3} lead={`Event Management`} />
          <CardServices
            icon={services4}
            lead={`Digital Communication Campaign`}
          />
        </Box>
        <Box className="grid grid-cols-1 gap-4 mt-4 lg:grid-cols-3 lg:mt-20">
          <CardCount icon={count1} count={"17"} lead={"Projects Completed"} />
          <CardCount icon={count2} count={"17"} lead={"Clients Happiness"} />
          <CardCount icon={count3} count={"5"} lead={"Country"} />
        </Box>
      </Box>

      {/* Our Clients */}
      <Box className="px-8 my-10 sm:px-20">
        <Box>
          <Typography className="text-xl sm:text-3xl font-bold">
            Our Clients
          </Typography>
          <Typography
            className="text-base sm:text-xl font-medium"
            sx={colorAccent1}
          >
            They entrusted for us, and all project the best finished.
          </Typography>
        </Box>
        <Box>
          <ClientCarousel imgCarousel={img} />
        </Box>
      </Box>

      {/* Our Team */}
      <Box className="px-8 my-10 sm:px-20">
        <Box className="mb-5 sm:mb-10">
          <Typography className="text-xl sm:text-3xl font-bold">
            Our Team
          </Typography>
          <Typography
            className="text-base sm:text-xl font-medium"
            sx={colorAccent1}
          >
            We are a team, we will keep your trust
          </Typography>
        </Box>
        <Box className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-4 gap-2 sm:gap-4 ">
          <CardProfile
            name="Anriko Caesar Yerico"
            title="Visual Communication Design"
            urlImage={teamImg1}
          />
          <CardProfile
            name="Dini Ang Dharmawan"
            title="Account Directur and Client Services"
            urlImage={teamImg2}
          />
          <CardProfile
            name="Adinda Joko Sanjoto"
            title="Strategic Communication and Organizer Development"
            urlImage={teamImg3}
          />
          <CardProfile
            name="Irvan Rahmat"
            title="Legal, Finance and Human Capital Development"
            urlImage={teamImg4}
          />
        </Box>
      </Box>
    </Box>
  );
}
