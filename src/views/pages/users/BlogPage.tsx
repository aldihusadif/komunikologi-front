import { Box, Typography } from "@mui/material";
import CardBlogPrimary from "src/ui-component/CardBlogPrimary";
import imgBlog from "src/assets/images/blog/blogImagePrimary.jpg";
import CardBlogSecondary from "src/ui-component/CardBlogSecondary";
import { colorSecondary } from "src/assets/styles/styles";
import imgActivity from "src/assets/images/blog/blogImagePrimary1.jpg";

export default function BlogPage() {
  const blogPrimary = {
    urlImage: imgBlog,
    titleBlog: "Shape a Bright Future with UNCRO in Papua!",
    leadBlog:
      "Introducing UNCRO (United Nations Community Reconciliation Organization), a beacon of hope for Papua. Together, we have the opportunity to mend the threads of peace, foster lasting collaborations, and carve a prosperous path for the people of Papua.",
    authorBlog: "Aldi Husadif",
    ocupationAuthor: "Software Engineer",
  };

  const blogSecondary = [
    {
      urlImage: imgBlog,
      titleBlog: "Shape a Bright Future with UNCRO in Papua!",
      authorBlog: "Aldi Husadif",
    },
    {
      urlImage: imgBlog,
      titleBlog: "Shape a Bright Future with UNCRO in Papua!",
      authorBlog: "Aldi Husadif",
    },
    {
      urlImage: imgBlog,
      titleBlog: "Shape a Bright Future with UNCRO in Papua!",
      authorBlog: "Aldi Husadif",
    },
  ];

  const allActivity = [
    {
      urlImage: imgBlog,
      titleBlog: "Shape a Bright Future with UNCRO in Papua!",
      leadBlog:
        "Introducing UNCRO (United Nations Community Reconciliation Organization), a beacon of hope for Papua. Together, we have the opportunity to mend the threads of peace, foster lasting collaborations, and carve a prosperous path for the people of Papua.",
      authorBlog: "Aldi Husadif",
      ocupationAuthor: "Software Engineer",
    },
    {
      urlImage: imgActivity,
      titleBlog: "Experience the Power of Global Communication",
      leadBlog:
        "Say goodbye to language barriers! App breaks down walls by enabling you to communicate effortlessly in English, the universal language of the modern world. Whether you're a student looking to enhance your language skills or a professional seeking to expand your global network, [App Name] is your trusted companion for seamless communication across borders.",
      authorBlog: "Aldi Husadif",
      ocupationAuthor: "Software Engineer",
    },
  ];

  return (
    <Box className="defaultContentPadding md:px-20">
      <Box className="grid grid-cols-1 gap-4 md:grid-cols-2">
        <Box>
          <CardBlogPrimary
            urlImage={blogPrimary.urlImage}
            titleBlog={blogPrimary.titleBlog}
            leadBlog={blogPrimary.leadBlog}
            authorBlog={blogPrimary.authorBlog}
            ocupationAuthor={blogPrimary.ocupationAuthor}
          />
        </Box>
        <Box>
          <Typography
            className="font-extrabold text-lg underline mb-2 lg:text-xl"
            sx={colorSecondary}
          >
            Recently Activity
          </Typography>
          <Box className="grid grid-rows-3 gap-2">
            {blogSecondary.map((item, index) => (
              <Box key={index}>
                <CardBlogSecondary
                  urlImage={item.urlImage}
                  titleBlog={item.titleBlog}
                  authorBlog={item.authorBlog}
                />
              </Box>
            ))}
          </Box>
        </Box>
      </Box>
      <Box className="mt-8">
        <Typography
          className="font-extrabold text-lg underline mb-4 lg:text-xl"
          sx={colorSecondary}
        >
          All Activity
        </Typography>
        <Box className="grid grid-cols-1 gap-4 lg:grid-cols-4">
          {allActivity.map((item, index) => (
            <Box key={index}>
              <CardBlogPrimary
                leadBlog={item.leadBlog.substring(0, 100)}
                ocupationAuthor={item.ocupationAuthor}
                urlImage={item.urlImage}
                titleBlog={item.titleBlog}
                authorBlog={item.authorBlog}
              />
            </Box>
          ))}
        </Box>
      </Box>
    </Box>
  );
}
