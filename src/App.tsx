import { CssBaseline, StyledEngineProvider } from "@mui/material";
import "./App.css";
// import { ThemeProvider } from "@mui/system";
import Routes from "./routes";

function App() {
  return (
    <StyledEngineProvider injectFirst>
      {/* <ThemeProvider theme={""}> */}
      <CssBaseline />
      <Routes />
      {/* </ThemeProvider> */}
    </StyledEngineProvider>
  );
}

export default App;
