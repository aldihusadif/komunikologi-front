import { lazy } from "react";
import PageLayout from "src/layout/PageLayout";
import Loadable from "src/ui-component/Loadable";

// routing
const HomePage = Loadable(lazy(() => import("src/views/pages/users/HomePage")));
const BlogPage = Loadable(lazy(() => import("src/views/pages/users/BlogPage")));
const AboutUsPage = Loadable(
  lazy(() => import("src/views/pages/users/AboutUsPage"))
);

const UserRoutes = {
  path: "",
  element: <PageLayout />,
  children: [
    {
      path: "/",
      element: <HomePage />,
    },
    {
      path: "home",
      element: <HomePage />,
    },
    {
      path: "blog",
      element: <BlogPage />,
    },
    {
      path: "about-us",
      element: <AboutUsPage />,
    },
  ],
};

export default UserRoutes;
