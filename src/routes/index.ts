import { useRoutes } from "react-router-dom";
import UserRoutes from "./UserRoutes";
import AdminRoutes from "./AdminRoutes";
import AuthenticationRoutes from "./AuthenticationRoutes";

export default function ThemeRoutes() {
    return useRoutes([UserRoutes, AdminRoutes, AuthenticationRoutes])
}