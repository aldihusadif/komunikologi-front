import { Instagram, LinkedIn, WhatsApp } from "@mui/icons-material";
import { Box, IconButton, Link, Typography } from "@mui/material";

export default function Footer() {
  return (
    <Box className="toBgGradient px-8 py-10 md:px-20">
      <Box className="grid grid-cols-1 gap-4 sm:grid-cols-2 md:grid-cols-3">
        <Box className="mb-4">
          <Typography className="text-xl font-extrabold leading-normal mb-1">
            About Us
          </Typography>
          <Box className="grid grid-cols-1 gap-2">
            <Link className="linkFooter">Our Team</Link>
            <Link className="linkFooter">Projects</Link>
            <Link className="linkFooter">Who are we?</Link>
          </Box>
        </Box>
        <Box className="mb-4">
          <Typography className="text-xl font-extrabold leading-normal mb-1">
            Resources
          </Typography>
          <Box className="grid grid-cols-1 gap-2">
            <Link className="linkFooter">Testimonials</Link>
            <Link className="linkFooter" href="/blog">
              Blog
            </Link>
          </Box>
        </Box>
        <Box>
          <Typography className="text-xl font-extrabold leading-normal mb-1">
            Social Media
          </Typography>
          <Box>
            <IconButton color="inherit" className="mr-4">
              <Instagram />
            </IconButton>
            <IconButton color="inherit" className="mr-4">
              <LinkedIn />
            </IconButton>
            <IconButton color="inherit" className="mr-4">
              <WhatsApp />
            </IconButton>
          </Box>
        </Box>
      </Box>
    </Box>
  );
}
