import { Typography } from "@mui/material";
import NavItem from "../NavItem";
import { groupMenu } from "src/menu-items/user";

const NavGroup = ({ item }: { item: groupMenu }) => {
  const items = item.children?.map((menu) => {
    switch (menu.type) {
      case "item":
        return <NavItem key={item.id} item={menu} />;
      default:
        return (
          <Typography key={menu.id} variant="h6" color="error" align="center">
            Menu Items Error
          </Typography>
        );
    }
  });
  return <>{items}</>;
};

export default NavGroup;
