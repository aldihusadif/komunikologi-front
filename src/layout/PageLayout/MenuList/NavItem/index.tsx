import { Button, ListItemButton, ListItemText } from "@mui/material";
import { forwardRef } from "react";
import { Link } from "react-router-dom";
import { colorSecondary } from "src/assets/styles/styles";
import { itemMenu } from "src/menu-items/user";

const NavItem = ({ item }: { item: itemMenu }) => {
  let itemTarget = "_self";
  if (item.target) {
    itemTarget = "_blank";
  }

  const listItemProps = {
    component: forwardRef<HTMLAnchorElement>((props, ref) => (
      <Button className="bg-transparent text-slate-200 capitalize">
        <Link ref={ref} {...props} to={item.url} target={itemTarget} />
      </Button>
    )),
  };

  return (
    <ListItemButton {...listItemProps} sx={{ textAlign: "center" }}>
      <ListItemText primary={item.title} sx={colorSecondary} />
    </ListItemButton>
  );
};

export default NavItem;
