import {
  AppBar,
  Box,
  CssBaseline,
  Divider,
  Drawer,
  IconButton,
  Toolbar,
  Typography,
} from "@mui/material";
import { useState } from "react";
import { IoMenuOutline } from "react-icons/io5";
import MenuList from "./MenuList";
import { colorSecondary } from "src/assets/styles/styles";
import { Outlet } from "react-router-dom";
import Logo from "src/assets/images/Logo.svg";
import Footer from "./Footer";

interface Props {
  window?: () => Window;
}

const drawerWidth = 240;

const PageLayout = (props: Props) => {
  const { window } = props;
  const [mobileOpen, setMobileOpen] = useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen((prevState) => !prevState);
  };

  const drawer = (
    <Box onClick={handleDrawerToggle} sx={{ textAlign: "center" }}>
      <Box className="pl-4 py-4">
        <img src={Logo} alt="komunikologi-indonesia" />
      </Box>
      <Divider />
      <Box sx={{ display: "flex", flexDirection: "column", textAlign: "left" }}>
        <MenuList />
      </Box>
    </Box>
  );

  const container =
    window !== undefined ? () => window().document.body : undefined;
  return (
    <Box sx={{ display: "block" }}>
      <CssBaseline />
      <AppBar component="nav" className="px-2 bg-white md:px-14" elevation={0}>
        <Toolbar>
          <IconButton
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            sx={{ mr: 2, display: { sm: "none" }, color: colorSecondary }}
          >
            <IoMenuOutline />
          </IconButton>
          <Typography
            variant="h6"
            component="div"
            sx={{
              flexGrow: 1,
              display: { xs: "none", sm: "block" },
              color: colorSecondary,
            }}
          >
            <img src={Logo} alt="komunikologi-indonesia" />
          </Typography>
          <Box sx={{ display: { xs: "none", sm: "block" } }}>
            <MenuList />
          </Box>
        </Toolbar>
      </AppBar>
      <Box component="nav">
        <Drawer
          container={container}
          variant="temporary"
          open={mobileOpen}
          onClose={handleDrawerToggle}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
          sx={{
            display: { xs: "block", sm: "none" },
            "& .MuiDrawer-paper": {
              boxSizing: "border-box",
              width: drawerWidth,
            },
          }}
        >
          {drawer}
        </Drawer>
      </Box>
      <Box component="main">
        <Toolbar />
        <Outlet />
        <Footer />
      </Box>
    </Box>
  );
};

export default PageLayout;
