const config = {
    basename: '/',
    defaultPath: '/home',
    fontFamily: `'Roboto', sans-serif`,
    borderRadius: 20
};

export default config;
